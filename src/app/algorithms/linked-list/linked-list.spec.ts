import { LinkedList } from './linked-list';

describe('LinkedList', () => {
    it('should create an instance', () => {
        expect(new LinkedList()).toBeTruthy();
    });

    it('should successfully insert', () => {
        const list = new LinkedList();

        list.insert(1);
        list.insert(2);
        list.insert(3);

        expect(list.getEntryCount()).toBe(3);
    });

    it('should search correctly', () => {
        const list = new LinkedList();

        list.insert(5);
        list.insert(10);
        list.insert(12);
        list.insert(-1);

        expect(list.search(5).getValue()).toBe(5);
        expect(list.search(6)).toBe(null);
    });

    it('should delete correctly', () => {
        const list = new LinkedList();

        list.insert(5);
        list.insert(10);
        list.insert(12);
        list.insert(-1);

        expect(list.delete(12)).toBeTruthy();
        expect(list.search(-1).getNext().getValue()).toBe(10);
        expect(list.getEntryCount()).toBe(3);

        expect(list.delete(-1)).toBeTruthy();
        expect(list.getEntryCount()).toBe(2);

        expect(list.delete(5)).toBeTruthy();
        expect(list.search(10).getNext()).toBe(null);
        expect(list.getEntryCount()).toBe(1);
    });

    it('should get list of entries', () => {
        const list = new LinkedList();

        list.insert(5);
        list.insert(10);
        list.insert(12);
        list.insert(-1);

        expect(list.rTraverse()).toEqual([-1, 12, 10, 5]);
        expect(list.traverse()).toEqual([5, 10, 12, -1]);
    });
});
