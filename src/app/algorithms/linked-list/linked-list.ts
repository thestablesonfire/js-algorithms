import { ListNode } from './list-node/list-node';

export class LinkedList {
    private list: ListNode[];
    private tail: ListNode;

    constructor() {
        this.list = [];
        this.tail = null;
    }

    delete(value: number): boolean {
        const parentNode = this.preSearch(value);

        if (parentNode.getValue() === value) {
            this.tail = parentNode.getNext();

            return true;
        } else {
            const child = parentNode.getNext();

            if (child) {
                parentNode.setNext(parentNode.getNext().getNext());
            }

            return !!child;
        }
    }

    getEntryCount(): number {
        let count = 0;
        let thisNode = this.tail;

        while (thisNode) {
            count++;
            thisNode = thisNode.getNext();
        }

        return count;
    }

    insert(value: number) {
        const newNode = new ListNode(this.tail, value);

        this.list.push(newNode);
        this.tail = newNode;
    }

    search(value: number): ListNode {
        const result = this.preSearch(value);

        return result.getValue() === value ? result : result.getNext();
    }

    traverse(): number[] {
        return this.rTraverse().reverse();
    }

    rTraverse(): number[] {
        const entries = [];
        let thisNode = this.tail;

        while (this.hasValidNext(thisNode)) {
            entries.push(thisNode.getValue());
            thisNode = thisNode.getNext();
        }

        entries.push(thisNode.getValue());

        return entries;
    }

    private preSearch(value: number) {
        let thisNode = this.tail;

        if (this.tail.getValue() === value) {
            return this.tail;
        }

        while (this.hasValidNext(thisNode) && !this.nextIsValue(thisNode, value)) {
            thisNode = thisNode.getNext();
        }

        return thisNode;
    }

    private hasValidNext(thisNode: ListNode): boolean {
        return thisNode !== null && !!thisNode.getNext();
    }

    private nextIsValue(thisNode: ListNode, value: number) {
        return thisNode.getNext().getValue() === value;
    }
}
