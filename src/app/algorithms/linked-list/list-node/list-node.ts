export class ListNode {
    constructor(private next: ListNode, private value: number) {}

    getNext(): ListNode {
        return this.next;
    }

    getValue(): number {
        return this.value;
    }

    setNext(next: ListNode): ListNode {
        this.next = next;

        return this;
    }

    setValue(value: number): ListNode {
        this.value = value;

        return this;
    }
}
