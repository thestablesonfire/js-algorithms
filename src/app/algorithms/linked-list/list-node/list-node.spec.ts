import { ListNode } from './list-node';

describe('ListNode', () => {
    it('should create an instance', () => {
        expect(new ListNode(null, 1)).toBeTruthy();
    });

    it('should properly get and set', () => {
        const node1 = new ListNode(null, 1);
        const node2 = new ListNode(node1, 2);
        const node3 = new ListNode(node2, 3);

        // Constructor values
        expect(node1.getValue()).toBe(1);
        expect(node2.getValue()).toBe(2);
        expect(node1.getNext()).toBe(null);
        expect(node2.getNext()).toBe(node1);

        // Set next
        expect(node2.setNext(node3).getNext()).toBe(node3);
        expect(node2.setNext(null).getNext()).toBe(null);

        // Set value
        expect(node2.setValue(6).getValue()).toBe(6);
    });
});
